<?php

namespace Drupal\codemirror_field;

use Symfony\Component\Finder\Finder;

/**
 * CodemirrorField class.
 *
 * A class that provides helper functions and settings for integrating
 * CodeMirror editor as an attachable field in Drupal.
 */
class CodemirrorField {

  /**
   * Helper function to get the library path root path.
   */
  public static function libPath($subpath = '') {
    return 'libraries/codemirror5' . $subpath;
  }

  /**
   * Helper function to build a list of available mode options.
   */
  public static function getModeOptions() {
    $mode_names = array_keys(self::getMode());
    return array_combine($mode_names, $mode_names);
  }

  /**
   * Helper function to build a list of available mode options.
   */
  public static function getThemeCss($theme) {
    return self::libPath('/theme/') . $theme . '.css';
  }

  /**
   * Returns an array of available theme options.
   *
   * @return array
   *   An array of theme options.
   */
  public static function getThemeOptions() {
    $themesPath = self::libPath() . '/theme';

    $finder = new Finder();
    $finder->files()->in($themesPath)->name('*.css')->depth(0);

    $themes = [];
    foreach ($finder as $file) {
      $name = $file->getBasename('.css');
      $themes[$name] = $name;
    }

    $themes = ['default' => 'default'] + $themes;

    ksort($themes);

    return $themes;
  }

  /**
   * Returns a single codemirror mode or all codemirror modes.
   *
   * @param string $mode
   *   The mode identifier for which the results are returned.
   *
   * @return array
   *   If a mode is given the function returns the settings for that, otherwise
   *   it returns all modes.
   */
  public static function getMode($mode = NULL) {
    $modes = [
      'text/html' => [
        'components' => ['xml', 'htmlmixed'],
        'addons' => [
          'hint' => [
            ['type' => 'xml'],
            ['type' => 'html'],
          ],
          'edit' => [
            'matchbrackets' => [],
            'matchtags' => [],
          ],
          'fold' => ['xml-fold'],
        ],
        'extraKeys' => [
          "Ctrl-Space" => "autocomplete",
        ],
      ],

      'css' => [
        'components' => ['css'],
        'addons' => [
          'hint' => [
            ['type' => 'css'],
          ],
          'lint' => [
            ['type' => 'css'],
          ],
          'edit' => [
            'matchbrackets' => [],
          ],
        ],
        'extraKeys' => [
          "Ctrl-Space" => "autocomplete",
        ],
      ],

      'text/x-less' => [
        'components' => ['css'],
        'addons' => [
          'hint' => [
            ['type' => 'css'],
          ],
          'lint' => [
            ['type' => 'css'],
          ],
          'edit' => [
            'matchbrackets' => [],
          ],
        ],
        'extraKeys' => [
          "Ctrl-Space" => "autocomplete",
        ],
      ],

      'javascript' => [
        'components' => ['javascript'],
        'addons' => [
          'hint' => [
            ['type' => 'javascript'],
          ],
          'lint' => [
            ['type' => 'javascript'],
          ],
          'edit' => [
            'matchbrackets' => [],
          ],
        ],
        'extraKeys' => [
          "Ctrl-Space" => "autocomplete",
        ],
      ],
    ];

    return isset($mode) ? ($modes[$mode] ?? FALSE) : $modes;
  }

}
