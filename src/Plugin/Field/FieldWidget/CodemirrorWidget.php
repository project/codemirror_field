<?php

namespace Drupal\codemirror_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\codemirror_field\CodemirrorField;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'codemirror_field_widget_default' widget.
 *
 * @FieldWidget(
 *   id = "codemirror_field_widget_default",
 *   label = @Translation("Codemirror"),
 *   field_types = {
 *     "codemirror"
 *   }
 * )
 */
class CodemirrorWidget extends StringTextareaWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_settings = $this->getFieldSettings();
    $mode = $field_settings['mode'];

    $instance_key = 'codemirror-instance-' .
      Html::cleanCssIdentifier($mode)
      . '-' . $delta;

    $element['value']['#attributes']['class'][] = $instance_key;

    $field_settings += [
      'extraKeys' => [
        'Ctrl-Space' => 'autocomplete',
      ],
    ];

    $element['#prefix'] = '<div class="codemirror-instance ' . $instance_key . '">';
    $element['#suffix'] = '</div>';

    $element['#attached']['drupalSettings']['codemirror'][$instance_key]['config'] = $field_settings;

    $themes_path = CodemirrorField::libPath('/theme');

    $element['#attached']['drupalSettings']['codemirror']['themes_path'] = $themes_path;

    $dependencies = 'codemirror_field/drupal.codemirror.' . $field_settings['mode'];

    $element['#attached']['library'][] = 'codemirror_field/drupal.codemirror';
    $element['#attached']['library'][] = 'codemirror_field/drupal.codemirror_field';
    $element['#attached']['library'][] = $dependencies;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    $element = parent::errorElement($element, $violation, $form, $form_state);
    if ($element === FALSE) {
      return FALSE;
    }
    elseif (isset($violation->arrayPropertyPath[0])) {
      return $element[$violation->arrayPropertyPath[0]];
    }
    else {
      return $element;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rows' => '9',
      'summary_rows' => '3',
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

}
