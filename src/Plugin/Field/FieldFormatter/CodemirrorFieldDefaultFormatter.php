<?php

namespace Drupal\codemirror_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\codemirror_field\CodemirrorField;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'codemirror_field_formatter_default' formatter.
 *
 * @FieldFormatter(
 *   id = "codemirror_field_formatter_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "codemirror"
 *   }
 * )
 */
class CodemirrorFieldDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $themes_path = CodemirrorField::libPath('/theme');

    foreach ($items as $delta => $item) {

      $field_settings = $item->getFieldDefinition()->getSettings();
      // The display must be always read-only.
      $field_settings['readOnly'] = 'nocursor';

      $mode = $field_settings['mode'];
      $instance_key = 'codemirror-instance-' .
        Html::cleanCssIdentifier($mode)
        . '-' . $delta;

      $element = [
        '#type' => 'textarea',
        '#prefix' => '<div class="codemirror-instance ' . $instance_key . '">',
        '#suffix' => '</div>',
        '#value' => $item->value,
        '#attached' => [
          'drupalSettings' => [
            'codemirror' => [
              $instance_key => [
                'config' => $field_settings,
              ],
              'themes_path' => $themes_path,
            ],
          ],
        ],
      ];

      $dependencies = 'codemirror_field/drupal.codemirror.' . $field_settings['mode'];

      $element['#attached']['library'][] = 'codemirror_field/drupal.codemirror';
      $element['#attached']['library'][] = 'codemirror_field/drupal.codemirror_field';
      $element['#attached']['library'][] = $dependencies;
      $elements[$delta] = $element;
    }

    return $elements;
  }

}
