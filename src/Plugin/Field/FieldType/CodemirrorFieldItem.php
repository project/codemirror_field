<?php

namespace Drupal\codemirror_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\codemirror_field\CodemirrorField;

/**
 * Plugin implementation of the 'codemirror' field type.
 *
 * @FieldType(
 *   id = "codemirror",
 *   label = @Translation("Codemirror"),
 *   description = @Translation("This field stores source code with syntax highlighting"),
 *   category = @Translation("Text"),
 *   default_widget = "codemirror_field_widget_default",
 *   default_formatter = "codemirror_field_formatter_default"
 * )
 */
class CodemirrorFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'theme' => 'default',
      'mode' => 'text/html',
      'indentUnit' => 2,
      'smartIndent' => 1,
      'tabSize' => 4,
      'indentWithTabs' => 0,
      'electricChars' => 1,
      'autoClearEmptyLines' => 0,
      'lineWrapping' => 1,
      'lineNumbers' => 1,
      'lint' => 0,
      'gutter' => 0,
      'fixedGutter' => 0,
      'showCursorWhenSelecting' => 0,
      'readOnly' => 0,
      'matchBrackets' => 1,
      'cursorBlinkRate' => 530,
      'undoDepth' => 40,
      'autofocus' => 0,
      'dragDrop' => 1,
      'matchTags' => 0,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#default_value' => $this->getSetting('mode'),
      '#required' => TRUE,
      '#options' => CodemirrorField::getModeOptions(),
      '#description' => $this->t('The source code programming language. (e.g.: CSS, Less, Javascript, etc.)'),
    ];

    $element['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Syntax highlight theme'),
      '#default_value' => $this->getSetting('theme'),
      '#required' => TRUE,
      '#options' => CodemirrorField::getThemeOptions(),
    ];

    $element['indentUnit'] = [
      '#type' => 'number',
      '#title' => $this->t('Indent unit'),
      '#description' => $this->t('How many spaces a block (whatever that means in the edited language) should be indented. The default is 2.'),
      '#default_value' => $this->getSetting('indentUnit'),
      '#required' => TRUE,
      '#min' => 0,
    ];

    $element['smartIndent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Smart indent'),
      '#description' => $this->t('Whether to use the context-sensitive indentation that the mode provides (or just indent the same as the line before). Defaults to TRUE.'),
      '#default_value' => $this->getSetting('smartIndent'),
    ];

    $element['lint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Linter'),
      '#description' => $this->t('Defines an interface component for showing linting warnings, with pluggable warning sources. Defaults to FALSE.'),
      '#default_value' => $this->getSetting('lint'),
    ];

    $element['tabSize'] = [
      '#type' => 'number',
      '#title' => $this->t('Tab size'),
      '#description' => $this->t('The width of a tab character. Defaults to 4.'),
      '#default_value' => $this->getSetting('tabSize'),
      '#required' => TRUE,
      '#min' => 0,
    ];

    $element['indentWithTabs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Indent with tabs'),
      '#description' => $this->t('Whether, when indenting, the first N*tabSize spaces should be replaced by N tabs. Default is FALSE.'),
      '#default_value' => $this->getSetting('indentWithTabs'),
    ];

    $element['electricChars'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Electric characters'),
      '#description' => $this->t('Configures whether the editor should re-indent the current line when a character is typed that might change its proper indentation (only works if the mode supports indentation). Default is TRUE.'),
      '#default_value' => $this->getSetting('electricChars'),
    ];

    $element['autoClearEmptyLines'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto clear empty lines'),
      '#description' => $this->t('When turned on (default is off), this will automatically clear lines consisting only of whitespace when the cursor leaves them. This is mostly useful to prevent auto indentation from introducing trailing whitespace in a file.'),
      '#default_value' => $this->getSetting('autoClearEmptyLines'),
    ];

    $element['lineWrapping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Line wrapping'),
      '#description' => $this->t('Whether CodeMirror should scroll or wrap for long lines. Defaults to FALSE (scroll).'),
      '#default_value' => $this->getSetting('lineWrapping'),
    ];

    $element['lineNumbers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Line numbers'),
      '#description' => $this->t('Whether to show line numbers to the left of the editor.'),
      '#default_value' => $this->getSetting('lineNumbers'),
    ];

    $element['gutter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Gutter'),
      '#description' => $this->t('Can be used to force a "gutter" (empty space on the left of the editor) to be shown even when no line numbers are active.'),
      '#default_value' => $this->getSetting('gutter'),
    ];

    $element['fixedGutter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fixed gutter'),
      '#description' => $this->t('When enabled (off by default), this will make the gutter stay visible when the document is scrolled horizontally.'),
      '#default_value' => $this->getSetting('fixedGutter'),
    ];

    $element['showCursorWhenSelecting'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show cursor when selecting'),
      '#description' => $this->t('Whether the cursor should be drawn when a selection is active. Defaults to FALSE.'),
      '#default_value' => $this->getSetting('showCursorWhenSelecting'),
      '#weight' => -87,
    ];

    $element['readOnly'] = [
      '#type' => 'select',
      '#title' => $this->t('Read only'),
      '#description' => $this->t('This disables editing of the editor content by the user. If the special value "nocursor" is given (instead of simply TRUE), focusing of the editor is also disallowed.'),
      '#default_value' => $this->getSetting('readOnly'),
      '#options' => [
        0 => $this->t('Editable'),
        1 => $this->t('Read only with cursor enabled'),
        'nocursor' => $this->t('Read only and disable cursor'),
      ],
    ];

    $element['matchBrackets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Match brackets'),
      '#description' => $this->t('Determines whether brackets are matched whenever the cursor is moved next to a bracket.'),
      '#default_value' => $this->getSetting('matchBrackets'),
    ];

    $element['matchTags'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Match tags'),
      '#description' => $this->t('Highlights the start and end tag.'),
      '#default_value' => $this->getSetting('matchTags'),
    ];

    $element['cursorBlinkRate'] = [
      '#type' => 'number',
      '#title' => $this->t('Cursor blink rate'),
      '#description' => $this->t('Half-period in milliseconds used for cursor blinking. The default blink rate is 530ms.'),
      '#default_value' => $this->getSetting('cursorBlinkRate'),
      '#required' => TRUE,
    ];

    $element['undoDepth'] = [
      '#type' => 'number',
      '#title' => $this->t('Undo depth'),
      '#description' => $this->t('The maximum number of undo levels that the editor stores. Defaults to 40.'),
      '#default_value' => $this->getSetting('undoDepth'),
      '#required' => TRUE,
    ];

    $element['autofocus'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autofocus'),
      '#description' => $this->t('Can be used to make CodeMirror focus itself on initialization. Defaults to off.'),
      '#default_value' => $this->getSetting('autofocus'),

    ];

    $element['dragDrop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Drag and Drop'),
      '#description' => $this->t('Controls whether drag-and-drop is enabled. On by default.'),
      '#default_value' => $this->getSetting('dragDrop'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Source Code'))
      ->setRequired(TRUE);

    return $properties;
  }

}
