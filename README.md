[[_TOC_]]

### Introduction

CodeMirror Field is a module that integrates the CodeMirror editor as an attachable field. More information about CodeMirror can be found at [http://codemirror.net/](http://codemirror.net/).

### Main Features

- Attach code snippet fields to entities with a customizable field widget and formatter.
- Benefit from autocomplete functionality for the supported modes.

### Available Options

- The widget currently supports four modes: `text/html`, `css`, `less`, and `javascript`. It can be easily extended to other languages supported by CodeMirror.
- Each field instance can have its own unique syntax highlighting theme.
- For more detailed options, refer to the official CodeMirror documentation at [http://codemirror.net/doc/manual.html](http://codemirror.net/doc/manual.html). You'll find information about options such as indent unit, smart indent, indent with tabs, electric characters, auto clear empty lines, line wrapping, line numbers, gutter, fixed gutter, show cursor when selecting, read-only, cursor blink rate, undo depth, auto focus, and drag and drop.

### Installation

1. Extend your `composer.json` file with the following code:
    ```json
    "repositories": [
      {
        "type": "package",
        "package": {
          "name": "codemirror/codemirror5",
          "type": "drupal-library",
          "version": "5",
          "dist": {
            "url": "https://codemirror.net/5/codemirror.zip",
            "type": "zip"
          }
        }
      }
    ]
    ```
2. Require the library with Composer: `composer require codemirror/codemirror5`.
3. Enable the CodeMirror Field module.

### Maintainers
 - Valentin Zsigmond (vzsigmond) - https://www.drupal.org/u/vzsigmond
