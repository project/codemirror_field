(function (drupalSettings) {
  /**
   * CodeMirror behavior
   * @type {Object}
   */
  var attached = false; // Flag to track if behavior is already attached

  Drupal.behaviors.codemirror_field = {
    attach: function (context, settings) {
      if (attached) {
        return; // Behavior already attached, so exit
      }

      var controller = this;
      var textareas = Array.from(document.querySelectorAll('.field--type-codemirror textarea:not(.codemirror-processed)'));
      console.log('attached');
      textareas.forEach(function (textarea) {
        if (textarea.classList.contains('codemirror-processed')) {
          return;
        }

        var instance = Array.from(textarea.closest('.codemirror-instance').classList).find(function (className) {
          return className.indexOf('codemirror-instance-') === 0;
        });

        if (!instance) {
          return;
        }

        if (typeof settings.codemirror[instance].config !== 'undefined') {
          var config = settings.codemirror[instance].config;

          Object.keys(config).forEach(function (key) {
            var value = config[key];
            if (!isNaN(value)) {
              config[key] = parseInt(value);
            }
          });

          if (typeof config.lint !== 'undefined' && config.lint == 1) {
            config.lint = true;
            config.gutters = ["CodeMirror-lint-markers"];
          }

          if (typeof config.matchTags !== 'undefined' && config.matchTags == 1) {
            config.matchTags = {
              bothTags: true
            };
          }

          CodeMirror.fromTextArea(textarea, config);
        }

        Array.from(document.querySelectorAll('.CodeMirror')).forEach(function (cm) {
          if (cm.nextElementSibling) {
            cm.nextElementSibling.classList.remove('grippie');
          }
        });

        textarea.classList.add('codemirror-processed');
      });

      controller.loadThemes();

      attached = true; // Set the attached flag to true
    },

    loadThemes: function () {
      var themes_path = drupalSettings.codemirror.themes_path;

      Object.keys(drupalSettings.codemirror).forEach(function (instanceKey) {
        var instanceSettings = drupalSettings.codemirror[instanceKey];

        if (instanceKey.indexOf('codemirror-instance') !== 0) {
          return;
        }

        if (instanceSettings.config.theme === 'default') {
          return;
        }

        if (document.querySelector('link[data-cm-theme="' + instanceSettings.config.theme + '"]')) {
          return;
        }

        var theme_css = themes_path + '/' + instanceSettings.config.theme + '.css';
        var css = document.createElement('link');
        css.setAttribute('data-cm-theme', instanceSettings.config.theme);
        css.setAttribute('href', '/' + theme_css);
        css.setAttribute('type', 'text/css');
        css.setAttribute('rel', 'stylesheet');

        document.head.appendChild(css);
      });
    }
  };
}(drupalSettings));
